Source: libdirectory-scratch-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Xavier Guimard <yadd@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-tiny-perl
Build-Depends-Indep: libpath-class-perl,
                     libpath-tiny-perl,
                     libstring-random-perl,
                     libtest-pod-coverage-perl,
                     libtest-pod-perl,
                     perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdirectory-scratch-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdirectory-scratch-perl.git
Homepage: https://metacpan.org/release/Directory-Scratch

Package: libdirectory-scratch-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libpath-class-perl,
         libpath-tiny-perl
Recommends: libstring-random-perl
Multi-Arch: foreign
Description: easy-to-use self-cleaning scratch space
 When writing test suites for modules that operate on files,
 it's often inconvenient to correctly create a platform-independent
 temporary storage space, manipulate files inside it, then clean it
 up when the test exits. The inconvenience usually results in tests
 that don't work everywhere, or worse, no tests at all.
 .
 Directory::Scratch aims to eliminate that problem by making it easy to
 do things right.
